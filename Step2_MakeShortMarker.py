################################################################
# MetaMarker: a de novo pipeline to discover novel metagenomic biomarkers
# Copyright (C) <2017> Mohamad Koohi-Moghadam mkoohim [AT] gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
################################################################
import distutils.spawn
import glob
import os
import subprocess
import math
import argparse
import numpy
import sys
from scipy.stats import mannwhitneyu, ttest_ind
import FunctionPool
import time

# Args
VERSION = "1.0"
parser = argparse.ArgumentParser(description='MakeShortMarker: This script process the hash tables which are made using MakeHashTable script.'
                                             'Here we make a profile table for each case and control group and extract those k-mers which '
                                             'are statistically different in case and control groups. The selected k-mers will be assembled '
                                             'to generate the short-markers.')
parser.add_argument("--version", action="version", version="%(prog)s v" + VERSION)
# Input
grpInput = parser.add_argument_group('Input:')
grpInput.add_argument('--caseHash', type=str, dest='strcaseHashTables',
                      help='Enter the path of hash tables (NPZ files) which are extracted for case.')

grpInput.add_argument('--controlHash', type=str, dest='strcontrolHashTables',
                      help='Enter the path of hash tables (NPZ files) which are extracted for control.')

#Tools
grpPrograms = parser.add_argument_group('Programs:')
grpPrograms.add_argument('--spade', default=str((distutils.spawn.find_executable("spades.py"))).replace("/spades.py", ""), type=str, dest='strSpade',
                         help='Provide the path to Spades. Default call will be spades.py.')

# Output
grpOutput = parser.add_argument_group('Output:')
grpOutput.add_argument('--output', type=str, dest='strOut',
                       help='Enter the path and name of the output directory.')

# Parameters - Matching Settings
grpParam = parser.add_argument_group('Parameters:')
grpParam.add_argument('--threads', type=int, dest='iThreads', help='Enter the number of CPUs available SPAdes. The default value is 1.',
                      default=1)
grpParam.add_argument('--test', type=str, dest='strTestType',
                      help="Enter type of test 'wilcoxon' or 'ttest'. By default we used Wilcoxon Mann Whitney test.", default='wilcoxon')
grpParam.add_argument('--pvalue', type=float, dest='strPvalue',
                      help='Enter the pvalue threshold for statistical test. The default value is 0.05.', default=0.05)

# Check for args.
if len(sys.argv) == 1:
    parser.print_help()
    sys.stderr.write(
        "\nNo arguments were supplied to ProfileDiff. Please see the usage information above to determine what to "
        "pass to the program.\n")
    sys.exit(1)

args = parser.parse_args()

if len([x for x in (args.strcaseHashTables, args.strcontrolHashTables, args.strOut) if x is not None]) != 3:
    parser.error('All --caseHash, --controlHash and --output parameters must be given.')

# Check Dependencies
sys.stderr.write("***** Checking dependencies *****\n")
FunctionPool.checkForDependency(args.strSpade, "--help", "spades.py")
sys.stderr.write("***** You have all requirement tools! *****\n\n")

# Set the program parameters
windowSize = 0
baseFolderCase = args.strcaseHashTables
baseFolderControl = args.strcontrolHashTables
output = args.strOut
output = FunctionPool.check_create_dir(output)
hashtableCase = glob.glob(baseFolderCase + os.sep + "*.npz")
hashtableControl = glob.glob(baseFolderControl + os.sep + "*.npz")

# Check the hash folders for case and control
if len(hashtableCase) == 0:
    sys.stderr.write("We could not find any NPZ files in "+ baseFolderCase +" folder!\n")
    sys.exit(1)
if len(hashtableControl) == 0:
    sys.stderr.write("We could not find any NPZ files in "+ hashtableControl +" folder!\n")
    sys.exit(1)

#### Load the npz files of case group and generate the profile table for case samples
start_time = time.time()
firstFlag = True
sys.stderr.write("Loading Case hash tables...\n")
for hashTable in hashtableCase:
    sys.stderr.write("Working on file: " + hashTable + "\n")
    if firstFlag:
        profileTableCase = numpy.load(hashTable)["arr_0"]
        profileTableCase = profileTableCase.reshape(len(profileTableCase), 1)
        windowSize = int(math.log(len(profileTableCase),4))
        firstFlag = False
    else:
        temp = numpy.load(hashTable)["arr_0"]
        temp = temp.reshape(len(temp), 1)
        profileTableCase = numpy.hstack((profileTableCase, temp))

sys.stderr.write("CPU Time for Case: " + str((time.time() - start_time)/60.0) + "\n")
sys.stderr.write("All case hash tables have been loaded into the profile table.\n")


#### Load the npz files of control group and generate the profile table for control samples
start_time = time.time()
sys.stderr.write("Loading Control profiles...\n")
firstFlag = True
for hashTable in hashtableControl:
    sys.stderr.write("Working on file: " + hashTable + "\n")
    if firstFlag:
        profileTableControl = numpy.load(hashTable)["arr_0"]
        profileTableControl = profileTableControl.reshape(len(profileTableControl), 1)
        firstFlag = False
    else:
        temp = numpy.load(hashTable)["arr_0"]
        temp = temp.reshape(len(temp), 1)
        profileTableControl = numpy.hstack((profileTableControl, temp))

sys.stderr.write("CPU Time for Control: " + str((time.time() - start_time)/60.0) + "\n")
sys.stderr.write("All control hash tables have been loaded into the profile table.\n")

# Open two files to save the case and control enriched k-mers
caseEnrichedFASTA = open(output + os.sep + 'case_enriched_mers.fasta', 'w')
controlEnrichedFASTA = open(output + os.sep + 'control_enriched_mers.fasta', 'w')

# Run statistical test on each entry of the case and control profile table
sys.stderr.write("Start to do statistical test...\n")
totalEntries = len(profileTableCase)
for i in range(0, len(profileTableCase)):
    if i % 100000 == 0:
        sys.stderr.write(str(i) + "/"+str(totalEntries)+" entries have been checked!\n")
    caseVals = profileTableCase[i, :]
    controlVals = profileTableControl[i, :]
    meanCase = numpy.mean(caseVals)
    meanControl = numpy.mean(controlVals)
    if args.strTestType is 'wilcoxon':
        stat, pval = mannwhitneyu(caseVals, controlVals, alternative="two-sided", use_continuity=True)
    elif args.strTestType is 'ttest':
        stat, pval = ttest_ind(caseVals, controlVals)
    # If the pvalue is less than 0.05 the k-mer will be saved    
    if pval < args.strPvalue:
        if meanCase >= meanControl:
            caseEnrichedFASTA.write(">Reads_" + str(i) + "_" + str(pval) + "_" + str(meanCase) + "_" + str(meanControl) + "\n")
            caseEnrichedFASTA.write(FunctionPool.indexToSeq(i, windowSize) + "\n")
        else:
            controlEnrichedFASTA.write(
                ">Reads_" + str(i) + "_" + str(pval) + "_" + str(meanCase) + "_" + str(meanControl) + "\n")
            controlEnrichedFASTA.write(FunctionPool.indexToSeq(i, windowSize) + "\n")
# Close the files and delete the profile tables in RAM to release memory
caseEnrichedFASTA.close()
controlEnrichedFASTA.close()
del profileTableCase
del profileTableControl

# Assemble the k-mers to generate the short-markers
sys.stderr.write("Run spade to assemble the k-mers and generate short-markers!\n")
#Call spade on the case result
subprocess.call([args.strSpade + os.sep + "spades.py", "-s", os.path.realpath(caseEnrichedFASTA.name) , "-o", output + os.sep + 'Spades_case' , "-k", '9', '--only-assembler'])

#Select markers longer than window size for Case
FunctionPool.selectFasta(output + os.sep + 'Spades_case' + os.sep + "contigs.fasta", output + os.sep + "Short_Markers_case.fasta", windowSize)

subprocess.call([args.strSpade + os.sep + "spades.py", "-s", os.path.realpath(controlEnrichedFASTA.name) , "-o", output + os.sep + 'Spades_control' , "-k", '9', '--only-assembler'])

#Select markers longer than window size for Control
FunctionPool.selectFasta(output + os.sep + 'Spades_control' + os.sep + "contigs.fasta", output + os.sep + "Short_Markers_control.fasta", windowSize)

sys.stderr.write("CPU Time: " + str((time.time() - start_time)/60.0) + "\n")