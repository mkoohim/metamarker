################################################################
# MetaMarker: a de novo pipeline to discover novel metagenomic biomarkers
# Copyright (C) <2017> Mohamad Koohi-Moghadam mkoohim [AT] gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
################################################################

import glob
import os
import sys
import argparse
import numpy
from Bio.SeqIO.FastaIO import SimpleFastaParser
import time
import FunctionPool
start_time = time.time()

# Args
VERSION = "1.0"
parser = argparse.ArgumentParser(description='MakeHashTable: This program generates a hash table for each whole metagenome '
                                             'sample by slide a window on reads and find the normalized abundance of '
                                             'each k-mer. The default size of the window is 12 (k=12). Larger size of k need largener RAM memory. '
                                             'The generated hash table will be saved az an NPZ. The '
                                             'input file should be in Fasta format.')
parser.add_argument("--version", action="version", version="%(prog)s v" + VERSION)
# Input
grpInput = parser.add_argument_group('Input:')
grpInput.add_argument('--sample', type=str, dest='strSample',
                      help='Enter the path of the folder that contain FASTA read files.')

# Output
grpOutput = parser.add_argument_group('Output:')
grpOutput.add_argument('--output', type=str, dest='strOut',
                       help='Enter the path of the output directory. The final NPZ format hash table will be saved into this folder.')

grpOutput.add_argument('--npz', type=str, dest='strNPZ',
                       help='Enter the name of the output NPZ file. The default name is same as the input sample file name.')

# Parameters - Matching Settings
grpParam = parser.add_argument_group('Parameters:')
grpParam.add_argument('--window_size', type=int, dest='windowLength',
                      help='Enther the length of window. The default value is 12. If you increase the size of the '
                           'window you need more RAM to run the program. The window size should be an even number.', default=12)
grpParam.add_argument('--step_size', type=int, dest='samplingRate', help='Enter the sampling rate. The default '
                                                                           'value is 1. It means the window move '
                                                                           'through the reads one base by one base. '
                                                                           'Increasing this parameter may affect on '
                                                                           'accuracy but the pipeline will run '
                                                                           'faster.', default=1)

# Check for args.
if len(sys.argv) == 1:
    parser.print_help()
    sys.stderr.write(
        "\nNo arguments were supplied to MakeHashTable. Please see the usage information above to determine what to "
        "pass to the program.\n")
    sys.exit(1)

args = parser.parse_args()

if len([x for x in (args.strSample, args.strOut) if x is not None]) != 2:
    parser.error('Both of the --sample and --output must be given.')

if args.windowLength % 2 != 0:
    parser.error('window size should be an even value! It seems you chose an odd value!')

# Generate output directory    
outpuDir = FunctionPool.check_create_dir(args.strOut)

# Set program parameters
windowLength = args.windowLength
hashSize = pow(2,windowLength) * pow(2,windowLength)
samplingRate = args.samplingRate

# Set sample name
sampleName = str(args.strSample).split(os.sep)[-1]
if args.strNPZ is None:
    args.strNPZ = sampleName

# Read FASTA files of the sample
fastaFiles = glob.glob(args.strSample + os.sep + "*.fasta")
if len(fastaFiles) == 0:
    sys.stderr.write("We could not find any FASTA files in "+ args.strSample +" folder!\n")
    sys.exit(1)

# Record the number of reads
readCount = 0

# Initialize hash table
sampleHashTable = numpy.zeros(hashSize, dtype='f4')

# Slide window on all reads of the FASTA file to generate 
# abundance of k-mers and save them into the hash table
for file in fastaFiles:
    sys.stderr.write("Working on: " + file + "\n")
    with open(file) as handle:
        for values in SimpleFastaParser(handle):
            seq = values[1]
            range = len(seq) - (windowLength - 1)
            readCount += 1
            if readCount % 100000 == 0:
                sys.stderr.write("Reads No: " + str(readCount) + " have been finished.\n")
            slice = [seq[i:i + windowLength] for i in xrange(0, range, samplingRate)]
            for marker in slice:
                try:
                    index = FunctionPool.seqToIndex(marker)
                    sampleHashTable[index] += 1
                except:
                    pass

# Calculate the normalized abundance of eahc k-mer
sampleHashTable /= float(readCount)
sampleHashTable *= 1000000

# Save the hash table in npz file to use in future steps
outputNPZ = outpuDir + os.sep + args.strNPZ + ".npz"
numpy.savez_compressed(outputNPZ, sampleHashTable)

# Print information about the file
sys.stderr.write("Total reads: " + str(readCount) + "\n")
sys.stderr.write("CPU Time: " + str((time.time() - start_time)/60.0) + "\n")