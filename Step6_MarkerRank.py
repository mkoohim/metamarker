################################################################
# MetaMarker: a de novo pipeline to discover novel metagenomic biomarkers
# Copyright (C) <2017> Mohamad Koohi-Moghadam mkoohim [AT] gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
################################################################
import distutils.spawn
import glob
import os
import argparse
import numpy
import sys
from Bio import SeqIO
from scipy.stats import mannwhitneyu
from shutil import copyfile
import FunctionPool
import pandas
import subprocess
################################################################################
# Args
VERSION = "1.0"
parser = argparse.ArgumentParser(description='MarkerRank: This script find those markers which are different in case and control.'
                                             'Here we used wilcoxon-rank sum test to assign a p-value to each marker.'
                                             'Markers with p-values less than 0.05 (default) have been selected and then we used cd-hit to remove redundant'
                                             'markers (similarity>60%). Finally the markers will rank based on the p-values')

parser.add_argument("--version", action="version", version="%(prog)s v" + VERSION)
# Input
grpInput = parser.add_argument_group('Input:')
grpInput.add_argument('--case', type=str, dest='strCase',
                      help='Enter the path of the marker abundance result for case samples.')
grpInput.add_argument('--control', type=str, dest='strControl',
                      help='Enter the path of the marker abundance result for control samples.')
grpInput.add_argument('--marker', type=str, dest='strMarker',
                      help='Enter the path of .')
# Parameters - Matching Settings
grpParam = parser.add_argument_group('Parameters:')
grpParam.add_argument('--cdhit', default=str((distutils.spawn.find_executable("cd-hit"))).replace("/cd-hit", ""), dest='strCdHit', help='Enter the path of CD-HIT tool.')

# Output
grpOutput = parser.add_argument_group('Output:')
grpOutput.add_argument('--output', type=str, dest='strOut',
                       help='Enter the path of the output directory.')
# Parameters - Matching Settings
grpParam = parser.add_argument_group('Parameters:')

grpParam.add_argument('--pvalue', type=float, dest='strPval', help='Enter the cut-off for p-value ', default=0.05)
grpParam.add_argument('--filter', type=float, dest='strFilter', help='Enter the cut-off to exclude markers with average abundance score  less than this value. The default value is 1.', default=1)
grpParam.add_argument('--topMarker', type=int, dest='strTopMarker', help='Enter the number of top markrs which you like to see in output. '
                                                                      'The number of final marker maybe less than your input value as we remove'
                                                                      'redundunt markers from top markers. The default value is 500.', default=500)
grpParam.add_argument('--similarity', type=float, dest='strSimilarity', help='Enter the cut-off for cdhit similarity.', default=0.6)
grpParam.add_argument('--memory', type=int, dest='strMemory', help='Enter the memory usage by CD-Hit. The default value is 40000MB', default=4000)
grpParam.add_argument('--threads', type=int, dest='strThreads', help='Enter the number of threads for CD-Hit. The default value is 1.', default=1)
# Check for args.
if len(sys.argv) == 1:
    parser.print_help()
    sys.stderr.write(
        "\nNo arguments were supplied to BGCDiff. Please see the usage information above to determine what to pass to the program.\n")
    sys.exit(1)

############################################################################
args = parser.parse_args()

if len([x for x in (args.strCase, args.strControl, args.strMarker,args.strOut) if x is not None]) != 4:
    parser.error('All --case, --control, --marker and --output parameters must be given.')


sys.stderr.write("***** Checking dependencies *****\n")
FunctionPool.checkForDependency(args.strCdHit, "-h", "cd-hit-est")
sys.stderr.write("***** You have all requirement tools! *****\n\n")

resultFolderCase = args.strCase
resultFolderControl = args.strControl
outputFolder = args.strOut

outputFolder = FunctionPool.check_create_dir(outputFolder)


TempFolder = outputFolder + os.sep + "Temp"
TempFolder = FunctionPool.check_create_dir(TempFolder)



markerStatFile = open(TempFolder + os.sep + 'Stats_Markers.txt', 'w')
markerStatFile.write("Marker\tMean_Case" + "\tMean_Control" + "\tP-value_Greater\tP-value_Less\n")

pandaDic = {}
allDataDF = pandas.DataFrame()

def analyzeResult(sourceFolder, group,flagFirst):
    resultFiles = glob.glob(sourceFolder + os.sep + "*.csv")
    for file in resultFiles:
        currentSample = os.path.basename(file).replace(".csv", "")
        sys.stderr.write("Working on " + currentSample + "\n")
        currentDF = pandas.read_csv(file)
        # It seems this is a bug in Bowtie2! Sometimes it generates a column with "ID:bowtie2" and I need to remove
        # it and reindex the dataframe.
        currentDF = currentDF[currentDF.Marker != "ID:bowtie2"]
        currentDF = currentDF.reset_index(drop=True)
        if flagFirst:
            allDataDF.loc[:,'Marker']  = currentDF.loc[:,'Marker']
            #print currentDF.loc[:,'Marker']
            allDataDF.set_index('Marker')
            allDataDF.loc[:, group + "_" + currentSample] = currentDF.loc[:, 'TotalHitNorm']
            flagFirst = False
        else:
            allDataDF.loc[:, group + "_" + currentSample] = currentDF.loc[:, 'TotalHitNorm']

newIndex = analyzeResult(resultFolderCase, "Case", True)
newIndex = analyzeResult(resultFolderControl, "Control",False)

allDataDF = allDataDF.set_index('Marker').transpose()

as_list = allDataDF.index.tolist()
new_index = []
for item in as_list:
    if "Case" in item:
        new_index.append("Case")
    else:
        new_index.append("Control")
allDataDF['Group'] = new_index
sys.stderr.write("Calculate P-values based on Wilcoxon-rank sum test. It may take several minute.\n")
for col in allDataDF.columns:
    if 'Group' not in col:
        case = allDataDF.loc[allDataDF['Group'] == "Case", col]
        control = allDataDF.loc[allDataDF['Group'] == "Control", col]
        meanCase = numpy.mean(case.values)
        meanControl = numpy.mean(control.values)
        try:
            stat_greater, pval_greater = mannwhitneyu(case.values, control.values, alternative="greater")
            stat_less, pval_less = mannwhitneyu(case.values, control.values, alternative="less")
        except:
            pval = 1
        valResult = col +"\t" + str(meanCase) + "\t" + str(meanControl) + "\t" + str(pval_greater) + "\t" + str(pval_less)
        markerStatFile.write(valResult + "\n")


markerStatFile.close()

refDic = {}
for record in SeqIO.parse(args.strMarker, "fasta"):
    refDic[str(record.id)] = str(record.seq)

markerStatDF = pandas.read_csv(TempFolder + os.sep + 'Stats_Markers.txt', sep="\t")
caseStatDF = markerStatDF[markerStatDF['Marker'].str.contains("Case")].copy()
controlStatDF = markerStatDF[markerStatDF['Marker'].str.contains("Control")].copy()

caseStatDF = caseStatDF[(caseStatDF["P-value_Greater"] < args.strPval) & (caseStatDF["Mean_Case"] > args.strFilter) & (caseStatDF["Mean_Control"] > args.strFilter)]
controlStatDF = controlStatDF[(controlStatDF["P-value_Less"] < args.strPval) & (controlStatDF["Mean_Case"] > args.strFilter) & (controlStatDF["Mean_Control"] > args.strFilter)]

caseStatDF.sort_values("P-value_Greater", inplace=True)
controlStatDF.sort_values("P-value_Less", inplace=True)

if len(caseStatDF) > args.strTopMarker:
    caseStatDF = caseStatDF.head(n=args.strTopMarker)

if len(controlStatDF) > args.strTopMarker:
    controlStatDF = controlStatDF.head(n=args.strTopMarker)

caseMarkers = caseStatDF['Marker'].values
controlMarkers = controlStatDF['Marker'].values


fileToWrite = open(TempFolder + os.sep + "Selected_Case_Markers.fasta", "w")
for item in caseMarkers:
    seq = refDic[item]
    fileToWrite.write(">" + item + "\n")
    fileToWrite.write(seq + "\n")
fileToWrite.close()


fileToWrite = open(TempFolder + os.sep + "Selected_Control_Markers.fasta", "w")
for item in controlMarkers:
    seq = refDic[item]
    fileToWrite.write(">" + item + "\n")
    fileToWrite.write(seq + "\n")
fileToWrite.close()

listCommand = [args.strCdHit + os.sep + 'cd-hit', '-i', TempFolder + os.sep + "Selected_Case_Markers.fasta" , '-o', TempFolder + os.sep + 'cdhit_case.fasta', '-d', '0', '-T', str(args.strThreads), '-g', '1', '-M', str(args.strMemory), '-c', str(args.strSimilarity), '-n', '4', '-aL',str(args.strSimilarity)]
subprocess.check_call(listCommand)
copyfile(TempFolder + os.sep + 'cdhit_case.fasta', outputFolder + os.sep + 'Final_Marker_Case.fasta')

listCommand = [args.strCdHit + os.sep + 'cd-hit', '-i', TempFolder + os.sep + "Selected_Control_Markers.fasta" , '-o', TempFolder + os.sep + 'cdhit_control.fasta', '-d', '0', '-T', str(args.strThreads), '-g', '1', '-M', str(args.strMemory), '-c', str(args.strSimilarity), '-n', '4', '-aL',str(args.strSimilarity)]
subprocess.check_call(listCommand)
copyfile(TempFolder + os.sep + 'cdhit_control.fasta', outputFolder + os.sep + 'Final_Marker_Control.fasta')

selectedMarkerCase = {}
for record in SeqIO.parse(outputFolder + os.sep + 'Final_Marker_Case.fasta', "fasta"):
    selectedMarkerCase[str(record.id)] = str(record.seq)

selectedMarkerControl = {}
for record in SeqIO.parse(outputFolder + os.sep + 'Final_Marker_Control.fasta', "fasta"):
    selectedMarkerControl[str(record.id)] = str(record.seq)


caseStatDF = caseStatDF.drop('P-value_Less', 1)
caseStatDF = caseStatDF.rename(columns={'P-value_Greater': 'P-value'})
caseStatDF.insert(0, 'Rank', 0)
caseStatDF['FoldChange'] = caseStatDF['Mean_Case'] / caseStatDF['Mean_Control']
caseStatDF['MarkerLength'] = 0
caseStatDF['MarkerSeq'] = ''
rank = 1
for index, row in caseStatDF.iterrows():
    if row['Marker'] not in selectedMarkerCase.keys():
        caseStatDF = caseStatDF.drop(index)
    else:
        caseStatDF.set_value(index, 'Rank', rank)
        caseStatDF.set_value(index, 'MarkerLength', len(selectedMarkerCase[row['Marker']]))
        caseStatDF.set_value(index, 'MarkerSeq', selectedMarkerCase[row['Marker']])
        rank += 1

caseStatDF.to_csv(outputFolder + os.sep + "Final_Marker_Case.csv",index=False)


controlStatDF = controlStatDF.drop('P-value_Greater', 1)
controlStatDF = controlStatDF.rename(columns={'P-value_Less': 'P-value'})
controlStatDF.insert(0, 'Rank', 0)
controlStatDF['FoldChange'] = controlStatDF['Mean_Control'] / controlStatDF['Mean_Case']
controlStatDF['MarkerLength'] = 0
controlStatDF['MarkerSeq'] = ''
rank = 1
for index, row in controlStatDF.iterrows():
    if row['Marker'] not in selectedMarkerControl.keys():
        controlStatDF = controlStatDF.drop(index)
    else:
        controlStatDF.set_value(index, 'Rank', rank)
        controlStatDF.set_value(index, 'MarkerLength', len(selectedMarkerControl[row['Marker']]))
        controlStatDF.set_value(index, 'MarkerSeq', selectedMarkerControl[row['Marker']])
        rank += 1

controlStatDF.to_csv(outputFolder + os.sep + "Final_Marker_Control.csv",index=False)

tempDF = pandas.DataFrame()
for col in selectedMarkerCase.keys():
    tempDF[col] = allDataDF[col].values

for col in selectedMarkerControl.keys():
    tempDF[col] = allDataDF[col].values
tempDF["Group"] = allDataDF["Group"].values
tempDF.to_csv(outputFolder + os.sep + "Marker_Stats.csv",index=False)