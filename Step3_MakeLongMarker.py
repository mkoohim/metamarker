################################################################
# MetaMarker: a de novo pipeline to discover novel metagenomic biomarkers
# Copyright (C) <2017> Mohamad Koohi-Moghadam mkoohim [AT] gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
################################################################
import distutils.spawn
import glob
import os
import sys
import argparse
from Bio import SeqIO
import FunctionPool
import subprocess
from shutil import copyfile

# Args
VERSION = "1.0"
parser = argparse.ArgumentParser(description='MakeLongMarker: This script uses MakeShortMarker script output.'
                                             'We used USEARCH to extract those reads which have at least one short-marker as substring.'                                             
                                             'The selected reads will be assembled to generate a longer marker.')
parser.add_argument("--version", action="version", version="%(prog)s v" + VERSION)
# Input
grpInput = parser.add_argument_group('Input:')
grpInput.add_argument('--short_marker', type=str, dest='shortMarkerStr',
                      help='Enter the path of the short-marker file which is found from MakeShortMarker.')

grpInput.add_argument('--sample', type=str, dest='strSample',
                      help='Enter the path of the folder that contain FASTA files.')

# Output
grpOutput = parser.add_argument_group('Output:')
grpOutput.add_argument('--tmp', type=str, dest='strTmp', default=None,
                       help='Enter the path and name of the tmp directory.')

grpOutput.add_argument('--output', type=str, dest='strOut', default=None,
                       help='Enter the path and name of the output directory.')

#Tools
grpPrograms = parser.add_argument_group('Programs:')
grpPrograms.add_argument('--spade', default=str((distutils.spawn.find_executable("spades.py"))).replace("/spades.py", ""), type=str, dest='strSpade',
                         help='Provide the path to Spades. Default call will be spades.py.')
grpPrograms.add_argument('--usearch', default=str((distutils.spawn.find_executable("usearch"))).replace("/usearch", ""), dest='usearchStr', help='Enter the path of USEARCH tool. Please provide the full path of usearch. i.e. tools/usearch11.0.667_i86linux32')
grpPrograms.add_argument('--gt', default=str((distutils.spawn.find_executable("gt"))).replace("/gt", ""), dest='gtStr', help='Enter the gt tool path.')

# Parameters - Matching Settings
grpParam = parser.add_argument_group('Parameters:')

grpParam.add_argument('--threads', type=int, dest='iThreads', help='Enter the number of CPUs available for USEARCH and SPAdes. The default value is 1.',
                      default=1)
grpParam.add_argument('--id', type=float, dest='idStr', help='Enter similarity rate for the USEARCH. The default value is 0.95.',
                      default=0.95)
# Check the args.
if len(sys.argv) == 1:
    parser.print_help()
    sys.stderr.write(
        "\nNo arguments were supplied to ExpandMarker. Please see the usage information above to determine what to "
        "pass to the program.\n")
    sys.exit(1)
args = parser.parse_args()
if len([x for x in (args.shortMarkerStr, args.strSample, args.strTmp, args.strOut) if x is not None]) != 4:
    parser.error('All --short_marker, --sample, --tmp and --output parameters must be given.')

# Check Dependencies
# TODO: usearch also should be added
sys.stderr.write("***** Checking dependencies *****\n")
FunctionPool.checkForDependency(args.strSpade, "--help", "spades.py")
FunctionPool.checkForDependency(args.gtStr, "--help", "gt")
sys.stderr.write("***** You have all requirement tools! *****\n\n")

# Create temp and output directory
tempDir = FunctionPool.check_create_dir(args.strTmp)
outputDir = FunctionPool.check_create_dir(args.strOut)

# Get sample name and the FASTA files
sampleName = str(args.strSample).split(os.sep)[-1]
resultFiles = glob.glob(args.strSample + os.sep + "*.fasta")
if len(resultFiles) == 0:
    sys.stderr.write("We could not find any FASTA files in "+ args.strSample +" folder!\n")
    sys.exit(1)

# We used USEARCH 32bit which has limitation for the FASTA file size. 
# We checked the size and split those FASTA which are bigger than 3GB.
for file in resultFiles:
    dFileInMB = round(os.path.getsize(file) / 1048576.0, 1)
    if dFileInMB > 3072:
        subprocess.check_call([args.gtStr + os.sep + "gt", "splitfasta", "-numfiles", "3", file])
        os.remove(file)

# Extract reads from FASTA files which have overlap with one of the short-markers
resultFiles = glob.glob(args.strSample + os.sep + "*")
for file in resultFiles:
    sys.stderr.write("Working on: " + file + "\n")
    currentReadFile = os.path.basename(file).replace(".fasta", "")
    subprocess.check_call([args.usearchStr , "--usearch_global", file , "--db", args.shortMarkerStr, "--threads", str(args.iThreads), "--strand", "plus", "--id", str(args.idStr), "--matched", tempDir + os.sep + currentReadFile + ".fasta"])
    outputFile = open(tempDir + os.sep + currentReadFile + "_mod.txt", "w")
    for record in SeqIO.parse(tempDir + os.sep + currentReadFile + ".fasta", "fasta"):
        # We removed the reads shorter than 50 to get a better result in  assembly
        if len(record.seq) > 50:
            outputFile.write(">" + str(record.id) + "\n")
            outputFile.write(str(record.seq) + "\n")
    outputFile.close()
    os.remove(tempDir + os.sep + currentReadFile + ".fasta")
sys.stderr.write("Finish Usearch...\n")

# Concatenate all the short read files
sys.stderr.write("Concatenate all the short read files...\n")
command = "cat " + tempDir + os.sep + "* > " + tempDir + os.sep + sampleName + ".fasta"
subprocess.call(command, shell=True)

# Assemble the reads to generate long-markers
sys.stderr.write("Assemble the reads using Spade to generate long-markers...\n")
listCommand = [args.strSpade + os.sep + "spades.py", '-s', tempDir + os.sep + sampleName + ".fasta" , '-o', tempDir + os.sep + "spade", '--only-assembler', '-k', '21', '--cov-cutoff', '3', '-t', str(args.iThreads)]
subprocess.check_call(listCommand)
copyfile(tempDir + os.sep + "spade" + os.sep + 'K21' + os.sep + 'final_contigs.fasta', outputDir + os.sep + sampleName + '_K21.fasta')
