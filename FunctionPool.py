################################################################
# MetaMarker: a de novo pipeline to discover novel metagenomic biomarkers
# Copyright (C) <2017> Mohamad Koohi-Moghadam mkoohim [AT] gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
################################################################

import glob
import os
import sys
import re
from Bio import SeqIO
import subprocess

############### Configs for CIGAR parser ####################
sam_cigar_match_mismatch_indel_identifiers=["M","=","X","I","D"]
sam_md_field_identifier="MD:Z:"
sam_nd_field_identifier="NM:i:"
sam_sn_field_identifier="SN:"
sam_ln_field_identifier="LN:"
sam_start_optional_index=11
sam_cigar_index=5
sam_cluster_name_index = 2
############################################


def check_create_dir(strDir):
    if not os.path.exists(strDir):
        os.makedirs(strDir)
    return strDir


repls = {'A': '00', 'T': '01', 'C': '10', 'G': '11'}
def seqToIndex(maskSeq):
    for i, j in repls.iteritems():
        maskSeq = maskSeq.replace(i, j)
    return int(maskSeq, base=2)


def indexToSeq(index, maskLength):
    template = "0" * 2 * maskLength
    finalSeq = ''
    binStr = str(bin(index)[2:])
    binStr = template[len(binStr): 2 * maskLength] + binStr
    for i in range(0, 2 * maskLength - 1, 2):
        if binStr[i:i + 2] == "00":
            finalSeq += "A"
        elif binStr[i:i + 2] == "01":
            finalSeq += "T"
        elif binStr[i:i + 2] == "10":
            finalSeq += "C"
        else:
            finalSeq += "G"
    return finalSeq


def selectFasta(input, output, thresold):
    outfile = open(output, 'w')
    for record in SeqIO.parse(input, "fasta"):
        if len(record.seq) > thresold:
            outfile.write(">" + str(record.id) + "\n")
            outfile.write(str(record.seq) + "\n")
    outfile.close()


def mergeFasta(input, output, lable):
    resultFiles = glob.glob(input + os.sep + "*.fasta")
    if len(resultFiles) == 0:
        sys.stderr.write("We could not find any FASTA files in " + input + " folder!\n")
        sys.exit(1)
    counter = 0
    fileToWrite = open(output + os.sep + "final_" + lable + ".fasta", "w")
    for file in resultFiles:
        currentSample = os.path.basename(file).replace(".fasta", "")
        sys.stderr.write("Working on: " + file + "\n")
        for record in SeqIO.parse(file, "fasta"):
            if len(record.seq) > 100:
                counter += 1
                fileToWrite.write(">" + currentSample + "_" + str(counter) + "\n")
                fileToWrite.write(str(record.seq) + "\n")
    return os.path.realpath(fileToWrite.name)


def mergeTwoFasta(file1, file2, output):
    fileToWrite = open(output, "w")
    for record in SeqIO.parse(file1, "fasta"):
        fileToWrite.write(">" + str(record.id) + "\n")
        fileToWrite.write(str(record.seq) + "\n")

    for record in SeqIO.parse(file2, "fasta"):
        fileToWrite.write(">" + str(record.id) + "\n")
        fileToWrite.write(str(record.seq) + "\n")
    fileToWrite.close()
    return os.path.realpath(fileToWrite.name)


def CDHitParser(inputFile, refFile, output, lable, repeatedRate):
    sys.stderr.write("Parse CD-Hit result for " + lable + ". It may take several minute!\n")
    refDic = {}
    for record in SeqIO.parse(refFile, "fasta"):
        refDic[str(record.id)] = str(record.seq)

    dicCluster = {}
    starId = {}
    with open(inputFile) as myfile:
        for line in myfile:
            line = line.rstrip()
            if ">Cluster" in line:
                dicCluster[line] = []
                starId[line] = ""
                currentHeader = line
                shortest = 0
            else:
                id = line.split("\t")[1].split(", >")[1].split("... ")[0].split("_")[0]
                length = int(line.split("\t")[1].split(", >")[0].replace("nt", ""))
                dicCluster[currentHeader] += [id]
                if length > shortest:
                    starId[currentHeader] = line.split("\t")[1].split(", >")[1].split("... ")[0]
                    shortest = length
    for item in dicCluster:
        dicCluster[item] = set(dicCluster[item])
    listContigs = []
    for item in dicCluster:
        if len(dicCluster[item]) >= repeatedRate:
            listContigs.append(starId[item])
    fileToWrite = open(output + os.sep + "Selected_Marker_" + lable + ".fasta", "w")
    counter = 0
    for item in listContigs:
        seq = refDic[item]
        fileToWrite.write(">" + lable + str(counter) + "\n")
        fileToWrite.write(seq + "\n")
        counter += 1
    fileToWrite.close()
    return os.path.realpath(fileToWrite.name)


def calculate_identity(cigar_string, md_field):
    match_numbers = re.compile("\d+")
    match_non_numbers = re.compile("\D+")

    # find the sets of numbers and identifers from the cigar string
    cigar_numbers = match_numbers.findall(cigar_string)
    cigar_identifiers = match_non_numbers.findall(cigar_string)

    # find the index for all of the match/mismatch/insert/delete
    match_mismatch_indel_index = []
    for index, cigar_identifier in enumerate(cigar_identifiers):
        if cigar_identifier in sam_cigar_match_mismatch_indel_identifiers:
            match_mismatch_indel_index.append(index)

    # identify the total number of match/mismatch/indel
    try:
        match_mismatch_indel_count = sum([float(cigar_numbers[index]) for index in match_mismatch_indel_index])
    except (IndexError, ValueError):
        match_mismatch_indel_count = 0.0

    md_field = md_field.replace(sam_nd_field_identifier, "")
    try:
        matches = match_mismatch_indel_count - int(md_field)
    except ValueError:
        matches = 0.0

    identity = 0.0
    if match_mismatch_indel_count > 0.0:
        identity = matches / (match_mismatch_indel_count * 1.0)

    return identity, match_mismatch_indel_count


def nm_field_location(info):
    # Search the data, starting with the first optional column to find the md field
    md_field = ""
    for data in info[sam_start_optional_index:]:
        if re.match(sam_nd_field_identifier, data):
            md_field = data
            break

    return md_field


def checkForDependency(strCmd, strArg, strIntendedProgram):
    if (strCmd == None):
        raise IOError(
            "\nWe can not find " + strIntendedProgram + " in your path. Please check that the program is installed properly and the path is correct.\n")
        sys.exit(1)
    try:
        if (strArg != ""):
            pCmd = subprocess.Popen([strCmd + os.sep + strIntendedProgram, strArg], stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
        else:
            pCmd = subprocess.Popen([strCmd + os.sep + strIntendedProgram], stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
    except:
        raise IOError(
            "\nWe can not find " + strIntendedProgram + " in your path.\nPlease check that the program is installed and the path is correct.\nThe current path for this tool is: " + strCmd + os.sep + strIntendedProgram + "\n")
        sys.exit(1)

    if (pCmd.returncode is None):
        sys.stderr.write(
            "Tested " + strIntendedProgram + ". Appears to be working. The current path for this tool is: " + strCmd + os.sep + strIntendedProgram + "\n")
    else:
        sys.stderr.write(
            "Tested " + strIntendedProgram + " returned a nonzero exit code (typically indicates failure). Please check to ensure the program is working. Will continue running.\n")
        sys.exit(1)
