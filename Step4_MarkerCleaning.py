################################################################
# MetaMarker: a de novo pipeline to discover novel metagenomic biomarkers
# Copyright (C) <2017> Mohamad Koohi-Moghadam mkoohim [AT] gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
################################################################
import distutils.spawn
import os
import sys
import subprocess
import argparse
import FunctionPool

# Args
VERSION = "1.0"
parser = argparse.ArgumentParser(description='MarkerCleaning: This script will process the MarkerExtend output files. '
                                             'Here we first merge all markers into one FASTA file. We remove markers shorter than 100bp.'
                                             'We then used CD-Hit to cluster the markers and remove those markers which '
                                             'are redundant (90% similarity) also we removed those markers which are '
                                             'appeared in less than 10% of samples.')
parser.add_argument("--version", action="version", version="%(prog)s v" + VERSION)
# Input
grpInput = parser.add_argument_group('Input:')
grpInput.add_argument('--caseMarker', type=str, dest='strInputCase',
                      help='Enter the path of the folder that contain Case samples markers.')
grpInput.add_argument('--caseSize', type=int, dest='strCaseSize',
                      help='Enter the size of case group (number of samples in case). ')

grpInput.add_argument('--controlMarker', type=str, dest='strInputControl',
                      help='Enter the path of the folder that contain Control samples markers.')
grpInput.add_argument('--controlSize', type=int, dest='strControlSize',
                      help='Enter the size of control group (number of samples in control).')
# Output
grpOutput = parser.add_argument_group('Output:')
grpOutput.add_argument('--output', type=str, dest='strOut', default="",
                       help='Enter the path and name of the output directory.')

# Parameters - Matching Settings
grpParam = parser.add_argument_group('Parameters:')
grpParam.add_argument('--cdhit', default=str((distutils.spawn.find_executable("cd-hit"))).replace("/cd-hit", ""), dest='strCdHit', help='Enter the path of CD-HIT tool.')

grpParam.add_argument('--memory', type=int, dest='strMemory', help='Enter the memory usage by CD-Hit. The default value is 40000MB', default=4000)
grpParam.add_argument('--threads', type=int, dest='strThreads', help='Enter the number of threads for CD-Hit. The default value is 1.', default=1)

# Check for args.
if len(sys.argv) == 1:
    parser.print_help()
    sys.stderr.write(
        "\nNo arguments were supplied to MarkerCleaning. Please see the usage information above to determine what to "
        "pass to the program.\n")
    sys.exit(1)
args = parser.parse_args()
if len([x for x in (args.strInputCase, args.strInputControl, args.strOut,args.strControlSize, args.strCaseSize) if x is not None]) != 5:
    parser.error('All --caseMarker, --controlMarker, --caseSize, --controlSize and --output parameters must be given.')

# Check Dependencies
sys.stderr.write("***** Checking dependencies *****\n")
FunctionPool.checkForDependency(args.strCdHit, "-h", "cd-hit-est")
sys.stderr.write("***** You have all requirement tools! *****\n\n")

# Create temp and output folders
outputDir = FunctionPool.check_create_dir(args.strOut)
tempCase = FunctionPool.check_create_dir(outputDir + os.sep + "temp_case")
tempControl = FunctionPool.check_create_dir(outputDir + os.sep + "temp_control")

# Merge all the long-markers which are extracted from the different samples
caseFilePath = FunctionPool.mergeFasta(args.strInputCase, tempCase, "Case")
controlFilePath = FunctionPool.mergeFasta(args.strInputControl, tempControl, "Control")

# Remove redundant long-markers
listCommand = [args.strCdHit + os.sep + 'cd-hit-est', '-i', caseFilePath , '-o', tempCase + os.sep + 'out_cdhit_case.fasta', '-d', '0', '-T', str(args.strThreads), '-g', '1', '-M', str(args.strMemory)]
subprocess.check_call(listCommand)
listCommand = [args.strCdHit + os.sep + 'cd-hit-est', '-i', controlFilePath , '-o', tempControl + os.sep + 'out_cdhit_control.fasta', '-d', '0', '-T', str(args.strThreads), '-g', '1', '-M', str(args.strMemory)]
subprocess.check_call(listCommand)

# Pars the CD-HIT output to remove those long-markers which are repeated in less than 0.1 portion of the samples.
# TODO: 0.1 should be parameterized
caseFilePath = FunctionPool.CDHitParser(tempCase + os.sep + 'out_cdhit_case.fasta.clstr', tempCase + os.sep + 'out_cdhit_case.fasta', tempCase, "Case", int(args.strCaseSize*0.1))
controlFilePath = FunctionPool.CDHitParser(tempControl + os.sep + 'out_cdhit_control.fasta.clstr', tempControl + os.sep + 'out_cdhit_control.fasta', tempControl, "Control", int(args.strControlSize*0.1))

# Merge the case and control enriched long-markers
mergedFile = FunctionPool.mergeTwoFasta(caseFilePath, controlFilePath, outputDir + os.sep + "Selected_Markers.fasta")
