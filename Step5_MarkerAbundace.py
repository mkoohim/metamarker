#!/usr/bin/env python
################################################################
# MetaMarker: a de novo pipeline to discover novel metagenomic biomarkers
# Copyright (C) <2017> Mohamad Koohi-Moghadam mkoohim [AT] gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
################################################################
import distutils.spawn
import glob
import sys
import argparse
import subprocess
import os
import pandas
import shutil
import FunctionPool
import time

# Args
VERSION = "1.0"
parser = argparse.ArgumentParser(description='MarkerAbundance: This script find the normalized abundance of each marker'
                                             'in WMS sample. The output will be saved into a CSV format file. ')
parser.add_argument("--version", action="version", version="%(prog)s v" + VERSION)
# Input
grpInput = parser.add_argument_group('Input:')
grpInput.add_argument('--marker', type=str, dest='strMarker',
                      help='Enter the path of the marker that you want to search, in FASTA format.')
grpInput.add_argument('--sample', type=str, dest='strSample',
                      help='Enter the path of the folder that contain reads. The read files should be in Fasta format.')

# Output
grpOutput = parser.add_argument_group('Output:')
grpOutput.add_argument('--tmp', type=str, dest='strTmp',
                       help='Enter the path of the tmp directory.')

grpOutput.add_argument('--output', type=str, dest='strOut',
                       help='Enter the path of the output directory.')

grpPrograms = parser.add_argument_group('Programs:')
grpPrograms.add_argument('--bowtie2', default=str((distutils.spawn.find_executable("bowtie2"))).replace("/bowtie2", ""), type=str, dest='strBowtie2',
                         help='Provide the path to bowtie2')
# Parameters - Matching Settings
grpParam = parser.add_argument_group('Parameters:')
grpParam.add_argument('--id', type=float, dest='dID', help='Enter the percent identity for the match', default=0.95)
grpParam.add_argument('--alnlength', type=float, dest='AlnLength',
                      help='Enter the minimum alignment length. The default is 50', default=60)
grpParam.add_argument('--threads', type=int, dest='iThreads', help='Enter the number of CPUs available for Bowtie2.',
                      default=1)
grpParam.add_argument('--removeTemp', action='store_true', dest='strRemoveTemp', help='If you use this option, the temp folder will be deleted from your disk.')

# Check for args.
if len(sys.argv) == 1:
    parser.print_help()
    sys.stderr.write(
        "\nNo arguments were supplied to MarkerAbundance. Please see the usage information above to determine what to "
        "pass to the program.\n")
    sys.exit(1)
args = parser.parse_args()
if (args.strMarker == None or args.strSample == None):
    parser.print_help()
    raise Exception("Command line arguments incorrect, must provide:\n" +
                    "\t--marker AND --sample, \n")
if (args.strOut == None):
    parser.print_help()
    raise Exception("Command line arguments incorrect, must provide:\n" +
                    "\t--output, \n")

# Check Dependencies
sys.stderr.write("***** Checking dependencies *****\n")
FunctionPool.checkForDependency(args.strBowtie2, "-h", "bowtie2")
sys.stderr.write("***** You have all requirement tools! *****\n\n")

# Create temp and output folder
if os.path.isdir(args.strOut):
    dirOut = args.strOut
    dirOut = FunctionPool.check_create_dir(dirOut)
    dirOut = os.path.abspath(dirOut)
else:
    raise Exception("Provide a folder for the output option (--output).\n")

dirTmp = args.strTmp
if(dirTmp==None):
    dirTmp = (dirOut + os.sep + "tmp" + str(os.getpid()) + '%.0f' % round((time.time()*1000), 1))

dirTmp = FunctionPool.check_create_dir(dirTmp)
dirTmp = os.path.abspath(dirTmp)

# Build Bowtie DB for the long-markers which we found
markerDbpath = ""
if args.strMarker != None:
    sys.stderr.write("Build the Bowtie2 db...\n")
    bowtieDBFolder = dirTmp + os.sep + "IndexDB"
    bowtieDBFolder = FunctionPool.check_create_dir(bowtieDBFolder)
    bowtieDBFolder = os.path.abspath(bowtieDBFolder)
    FNULL = open(os.devnull, 'w')
    subprocess.call([args.strBowtie2 + os.sep + 'bowtie2-build', '-f',args.strMarker,bowtieDBFolder + os.sep + "MarkerDB"] , stdout=FNULL, stderr=subprocess.STDOUT)
    markerDbpath = bowtieDBFolder + os.sep + "MarkerDB"
else:
    markerDbpath = args.strMarkerIndex

start_time = time.time()
currentSample = str(args.strSample).split(os.sep)[-1]
readNumber = 0
# Generate folders for intermediate results
sampleTemp = dirTmp + os.sep + currentSample
sampleTemp = FunctionPool.check_create_dir(sampleTemp)
sampleTemp = os.path.abspath(sampleTemp)

resultFiles = glob.glob(args.strSample + os.sep + "*.fasta")
if len(resultFiles) == 0:
    sys.stderr.write("We could not find any FASTA files in "+ args.strSample +" folder!\n")
    sys.exit(1)

strFiles = ""
for file in resultFiles:
    strFiles += file + ","
strFiles = strFiles.rstrip(",")

sys.stderr.write("Run Bowtie2..." + currentSample + "\n")
bowtieOut = open(sampleTemp + os.sep + "temp.txt", "w")
subprocess.call([args.strBowtie2 + os.sep + "bowtie2", "--no-unal", "-f", "-x", markerDbpath , "-U", strFiles, "-S", sampleTemp + os.sep + "out_bowtie.sam", "-p", str(args.iThreads)], stderr=bowtieOut)
bowtieOut.close()
bowtieOut = open(sampleTemp + os.sep + "temp.txt", "r")

# To save time, we extract the number of reads from the Bowtie output
findIt = False
for line in bowtieOut:
    sys.stderr.write(line + "\n")
    if "reads; of these:" in line:
        findIt = True
        readNumber = int(line.split(" ")[0])
if not findIt:
    raise Exception("Bowtie2 could not generate result for sample:" + currentSample)
bowtieOut.close()

# Process the Bowtie output to find the number of hits.
sys.stderr.write("Process the CIGAR..." + currentSample + "\n")
currentFile = sampleTemp + os.sep + "out_bowtie.sam"
excludedFile = open(sampleTemp + os.sep + 'out_cigar.sam', 'w')

# TODO: This part can be moved to the FunctionPool
clusterDic = {}
with open(currentFile, "r") as result:
    for line in result:
        if "@" in line:
            excludedFile.write(line)
            if "SQ" in line:
                clusterName = line.split("\t")[1].replace(FunctionPool.sam_sn_field_identifier, "")
                clusterLength = line.split("\t")[2].replace(FunctionPool.sam_ln_field_identifier, "")
                clusterDic[clusterName] = 0
        else:
            info = line.split("\t")
            cigar_str = info[FunctionPool.sam_cigar_index]
            identity, alignLength = FunctionPool.calculate_identity(cigar_str,
                                                                    FunctionPool.nm_field_location(info))
            if identity >= args.dID and alignLength >= args.AlnLength:
                clusterName = info[FunctionPool.sam_cluster_name_index]
                clusterDic[clusterName] += 1
                excludedFile.write(line)
excludedFile.close()

# Create a data frame and saved all the normalized abundance 
abstractDF = pandas.DataFrame()
abstractDF["TotalHit"] = 0
for item in clusterDic:
    abstractDF.set_value(item, "TotalHit", clusterDic[item])
abstractDF["TotalHitNorm"] = (abstractDF["TotalHit"] / readNumber) * 1000000
abstractDF.index.name = "Marker"

# Save the results into a CSV format
abstractDF.to_csv(dirOut + os.sep + currentSample + ".csv")
timeMin = (time.time() - start_time) / 60
sys.stderr.write("Time for sample " + currentSample + " " + str(timeMin) + "\n")

# Remove temp folder to release the space
if args.strRemoveTemp:
    shutil.rmtree(dirTmp)